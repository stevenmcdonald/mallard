module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:preact/recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    "overrides": [
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "preact",
        "@typescript-eslint"
    ],
    "rules": {
        "indent": ["error", 4],
        "react/jsx-indent-props": [2, 4],
        "brace-style": [1, "1tbs", {"allowSingleLine": true}],
        "react/jsx-closing-bracket-location": 1
    }
}
