import { describe, test, expect } from 'vitest';
import { render } from '@testing-library/preact';

import { App } from './app';

describe('<App />', () => {
    test('App renders', () => {
        const wrapper = render(<App />);
        expect(wrapper).toBeTruthy();
    });
});
