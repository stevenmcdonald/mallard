import { MallardPosts } from './components/MallardPosts';

import './css/reset.css';
import './app.css';

export function App() {
    return (
        <div class="content">
            <MallardPosts />
        </div>
    );
}
