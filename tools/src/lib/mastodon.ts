import { log } from './log';


// naems are from Mastodon, eslint has to deal
export interface AccountIface {
    id: string;
    avatar_static: string; // eslint-disable-line camelcase
    acct: string;
    display_name: string; // eslint-disable-line camelcase
    header_static: string; // eslint-disable-line camelcase
}

export interface MediaAttachmentIface {
    type: string;
    // the name if from the API, stop complaining
    // eslint-disable-next-line camelcase
    preview_url: string;
    description: string;
}

export interface PostIface {
    created_at: string; // eslint-disable-line camelcase
    favourites_count: number; // eslint-disable-line camelcase
    replies_count: number; // eslint-disable-line camelcase
    reblogs_count: number; // eslint-disable-line camelcase
    content: string;
    url: string;
    media_attachments: MediaAttachmentIface[]; // eslint-disable-line camelcase
    // [n: string]: string; // needed for the sorting in SortedPostsList
}

//
// returns an options object suitable for passing in to `fetch`
//
// Add our token, decline any other creditials (no cookies needed),
// and error on redirects for now.
//
export function getOptions(token: string) {
    const options: RequestInit = {
        headers: {
            Authorization: `Bearer ${token}`
        },
        credentials: 'omit',
        redirect: 'error'
    };
    return options;
}


//
// use the `verify_credentials` API to verify our token and get the user's
// user ID
//
export async function getUserInfo(server: string, token: string) {
    log.debug('verifying server', server);

    const url = `https://${server}/api/v1/accounts/verify_credentials`;
    const options = getOptions(token);

    const r = await fetch(url, options);
    const data = await r.json();

    return data;
}
