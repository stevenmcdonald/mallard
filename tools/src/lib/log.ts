
// function _log(level, ...args) {
//     // const callSite = new Error().stack!.split('\n')[2].trim(); // eslint-disable-line @typescript-eslint/no-non-null-assertion
//     // console[level](...args, callSite); // eslint-disable-line no-console
//     console.log.apply(args);  // eslint-disable-line no-console
// }

// class Log {
//     debug = (...args) => _log('debug', args);
//     error = (...args) => _log('error', args);
//     info = (...args) => _log('info', args);
//     log = (...args) => _log('log', args);
//     trace = (...args) => _log('trace', args);
//     warn = (...args) => _log('warn', args);
// }

// export const log = new Log();

import { Logger } from 'tslog';

export const log = new Logger({ name: 'mallardLogger' });
