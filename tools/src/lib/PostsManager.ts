
import { getOptions, PostIface } from './mastodon';

import { log } from '../lib/log';

interface ParamsI {
    only_media?: boolean; // eslint-disable-line camelcase
    exclude_replies?: boolean; // eslint-disable-line camelcase
    exclude_reblogs?: boolean; // eslint-disable-line camelcase
}

export class PostsManager {
    server;
    token;
    userID;
    posts: {[k: string]: PostIface[]} | {[k: string]: never};
    options;

    constructor(server: string, token: string, userID: string) {
        this.server = server;
        this.token = token;
        this.userID = userID;

        this.posts = {};

        this.options = {
            onlyMedia: false,
            excludeReplies: true,
            excludeBoosts: true
        };
    }

    onlyMedia(val: boolean) {
        this.options.onlyMedia = val;
        return (this);
    }

    excludeReplies(val: boolean) {
        this.options.excludeReplies = val;
        return (this);
    }

    excludeBoosts(val: boolean) {
        this.options.excludeBoosts = val;
        return (this);
    }

    getParams() {
        const params: ParamsI = {};

        if (this.options.onlyMedia) { params.only_media = true; }
        if (this.options.excludeReplies) { params.exclude_replies = true; }
        if (this.options.excludeBoosts) { params.exclude_reblogs = true; }

        // XXX apparently TS sucks at URLSearchParams?
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const p = new URLSearchParams(params as any);
        return (p.toString());
    }

    async getPosts() {
        const key = this.getParams();

        if (!Object.prototype.hasOwnProperty.call(this.posts, key)) {
            // we haven't loaded the posts for this set of API params
            log.debug(`Need to load posts for '${key}'`);
            const posts = await this.fetch();
            this.posts[key] = posts;
        }

        return this.posts[key];
    }

    async fetch() {
        const limit = 40;
        const url = `https://${this.server}/api/v1/accounts/${this.userID}/statuses?limit=${limit}&` +
            this.getParams();
        const options = getOptions(this.token);

        const r = await fetch(url, options);
        if (r.status != 200) {
            log.error('fetch error:', r.status);
            return r.status;
        }

        return await r.json();
    }
}
