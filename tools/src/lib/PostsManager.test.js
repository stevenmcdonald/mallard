import { beforeEach, describe, expect, test } from 'vitest';

import { PostsManager } from './PostsManager';


describe('PostsManager', () => {
    beforeEach(() => {
        fetch.resetMocks();
    });

    test('fetch posts', async () => {
        const mockPosts = [{ id: '1' }, { id: '2' }];

        fetch.mockResponse(JSON.stringify(mockPosts));

        const mgr = new PostsManager('example.social', 'mockToken', 'mockUserID');

        // note this sequence of tests depends on the state of mgr.options
        expect(mgr.getParams()).toBe('exclude_replies=true&exclude_reblogs=true');
        expect(mgr.excludeReplies(false).excludeBoosts(false).getParams())
            .toBe('');
        expect(mgr.onlyMedia(true).getParams()).toBe('only_media=true');

        // should be "?only_media=true"
        const posts = await mgr.getPosts();

        const expectedUrl = 'https://example.social/api/v1/accounts/mockUserID/statuses?limit=40&only_media=true';

        expect(posts).toStrictEqual(mockPosts);
        expect(fetch.requests().length).toEqual(1);
        expect(fetch.requests()[0].url).toEqual(expectedUrl);
    });

    test('fetch posts exception', async () => {
        fetch.mockResponse('<html><head></head><body>404\'d</body></html', {
            status: 404,
            statusText: 'Not Found'
        });
        const mgr = new PostsManager('example.social', 'mockToken', 'mockUserID');

        const expectedUrl = 'https://example.social/api/v1/accounts/mockUserID/statuses?limit=40&exclude_replies=true&exclude_reblogs=true';
        return mgr.fetch().then((status) => {
            expect(status).toBe(404);
            expect(fetch.requests().length).toEqual(1);
            expect(fetch.requests()[0].url).toEqual(expectedUrl);
        });
    });

});
