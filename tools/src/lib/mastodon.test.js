import { describe, beforeEach, expect, test } from 'vitest';

import { getOptions, getUserInfo } from './mastodon';


describe('Mastodon utils', () => {
    beforeEach(() => {
        fetch.resetMocks();
    });

    test('getOptions', () => {
        const options = getOptions('token');

        expect(options.credentials).toBe('omit');
        expect(options.redirect).toBe('error');
        expect(options.headers.Authorization).toBe('Bearer token');
    });

    test('getUserInfo', async () => {

        const mockData = {
            id: 'mockAccountID'
        };

        fetch.mockResponse(JSON.stringify(mockData));

        expect(await getUserInfo('example.social', 'mockToken')).toStrictEqual({ id: 'mockAccountID' });
        expect(fetch).toHaveBeenCalledWith(
            'https://example.social/api/v1/accounts/verify_credentials',
            getOptions('mockToken'));

        fetch.mockRestore();
    });
});
