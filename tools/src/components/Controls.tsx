import { StateUpdater } from 'preact/hooks';

import { FilterControls } from './FilterControls';
import { SortControls } from './SortControls';
import { SearchControls } from './SearchControls';
import { ToggleButton } from './ToggleButton';

import '../css/Controls.css';

interface ControlsProps {
    activeTab: string;
    setActiveTab: StateUpdater<string>;
    sortCol: string;
    sortDate: (_: Event) => null;
    sortStar: (_: Event) => null;
    sortBoost: (_: Event) => null;
    sortReply: (_: Event) => null;
    searchVal: string;
    setSearchVal: StateUpdater<string>;
    // thse call StateUpdaters but are wrapped in functions
    // mostly to invert the Replies logic
    includeReplies: boolean;
    setIncludeReplies: (v: boolean) => void;
    onlyMedia: boolean;
    setOnlyMedia: StateUpdater<boolean>;
}

export function Controls({
    activeTab, setActiveTab, sortCol, sortDate, sortStar, sortBoost, sortReply,
    searchVal, setSearchVal, includeReplies, setIncludeReplies, onlyMedia,
    setOnlyMedia
}: ControlsProps) {
    return (
        <div class="post-controls">
            <div class="mobile">
                <div class="post-controls-mobile">
                    <div>
                        <ToggleButton
                            options={['search', 'sort', 'filter']}
                            labels={['🔍', '🔀', '🔄']}
                            ariaLabels={['Search', 'Sort', 'Filter']}
                            activeOption={activeTab}
                            setOption={setActiveTab}
                        />
                    </div>
                    <div>
                        { activeTab == 'sort' && (
                            <SortControls
                                sortCol={sortCol}
                                sortDate={sortDate}
                                sortStar={sortStar}
                                sortBoost={sortBoost}
                                sortReply={sortReply}
                            />
                        )}
                        { activeTab == 'search' && (
                            <SearchControls
                                searchVal={searchVal}
                                setSearchVal={setSearchVal}
                                size={20}
                            />
                        )}
                        { activeTab == 'filter' && (
                            <FilterControls
                                includeReplies={includeReplies}
                                setIncludeReplies={setIncludeReplies}
                                onlyMedia={onlyMedia}
                                setOnlyMedia={setOnlyMedia}
                            />
                        )}
                    </div>
                </div>
            </div>
            <div class="desktop">
                <div class="post-controls-desktop-container">
                    <div class="post-controls-desktop">
                        <button class="control-label toggle-button">
                            Filter&nbsp;🔄
                        </button>
                        <FilterControls
                            includeReplies={includeReplies}
                            setIncludeReplies={setIncludeReplies}
                            onlyMedia={onlyMedia}
                            setOnlyMedia={setOnlyMedia}
                        />
                    </div>
                    <div class="post-controls-desktop">
                        <button class="control-label toggle-button">
                            Sort&nbsp;🔀
                        </button>
                        <SortControls
                            sortCol={sortCol}
                            sortDate={sortDate}
                            sortStar={sortStar}
                            sortBoost={sortBoost}
                            sortReply={sortReply}
                        />
                    </div>
                    <div class="post-controls-desktop">
                        <button class="control-label toggle-button">
                            Filter&nbsp;🔍
                        </button>
                        <SearchControls
                            searchVal={searchVal}
                            setSearchVal={setSearchVal}
                            size={42}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}