import { useCallback } from 'preact/hooks';

import { log } from '../lib/log';

import '../css/Navbar.css';

export function Navbar() {
    const logout = useCallback((e: Event) => {
        log.debug('logout', e);
        localStorage.removeItem('server');
        localStorage.removeItem('token');
    }, []);

    return (
        <div class="navbar">
            <form method="get" action="/" onSubmit={logout}>
                <button class="logout-button" type="submit" aria-label="Logout">
                    X
                </button>
            </form>
        </div>
    );
}
