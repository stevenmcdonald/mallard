
import { Loading } from './Loading';
import { PostsList } from './PostsList';
import { PostIface } from '../lib/mastodon';

interface SortedPostListProps {
    posts: PostIface[];
    sortCol: string;
    searchVal: string;
}

// from: https://stackoverflow.com/a/3561711
function escapeRegex(str: string) {
    return str.replace(/[/\-\\^$*+?.()|[\]{}]/g, '\\$&');
}

function filterPosts(
    posts: PostIface[], searchVal: string, useRegexp: boolean, caseSens: boolean
) {
    if (!searchVal) return posts;

    const sVal = useRegexp ? searchVal : escapeRegex(searchVal);
    const flags = caseSens ? '' : 'i';
    const re = new RegExp(sVal, flags);

    return posts.filter((p) => re.test(p.content));
}

export function SortedPostsList({
    posts, sortCol, searchVal
}: SortedPostListProps) {
    const filteredPosts = filterPosts(posts, searchVal, false, false);

    if (sortCol == 'created_at') {
        // 2 posts shouldn't have the exact same created_at, so this is lazy
        filteredPosts.sort((a, b) => a[sortCol] > b[sortCol] ? -1 : 1);
    } else {
        // XXX typescript is making this hard...
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        filteredPosts.sort((a: any, b: any) => b[sortCol] - a[sortCol]);
    }

    return (
        <>
            { (posts && posts.length)
                ? (<PostsList sortCol={sortCol} posts={filteredPosts} />)
                : (<Loading />)
            }
        </>
    );
}