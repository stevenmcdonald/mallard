import { useState, useCallback } from 'preact/hooks';

import '../css/Login.css';

export function Login() {
    const [server, setServer] = useState('');

    const onInput = useCallback((e: Event) => {
        const element = e.target as HTMLInputElement;
        setServer(element.value);
    }, []);

    const goTo = `/api/login/${server}/`;

    return (
        <div class="login-container">
            <div class="login">
                <div class="login-info">
                    <h3>🦆 Mallard Tools 🦆</h3>
                    <p>
                        I wanted to know which of my posts got the most boosts
                        and replies. I had trouble finding my recent posts at
                        all. I couldn't find any tools to help with with that…
                    </p>
                    <p>
                        So, I made this. This is very much a work in progress,
                        but it's reached the stage of being useful.
                    </p>
                    <p>Proceed with caution</p>
                    <p>
                        Only read access is requested for you account and none
                        of your login info is stored on the server.
                    </p>
                </div>
                <div class="login-form-container">
                    <form class="login-form" method="get" action={goTo}>
                        <div>
                            <label for="server" class="server-label">Mastodon server</label>
                        </div>
                        <div>
                            <span class="server-proto">https://</span>
                            <input
                                id="server"
                                type="text"
                                value={server}
                                onInput={onInput}
                            />
                        </div>
                        <button type="submit">Login</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
