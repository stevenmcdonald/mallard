import { PostMedia } from './PostMedia';

import { MediaAttachmentIface } from '../lib/mastodon';

import '../css/Post.css';

interface PostProps {
    sortCol: string;
    date: string;
    replies: number;
    boosts: number;
    stars: number;
    content: string;
    media: MediaAttachmentIface | null;
    mediaCount: number;
    url: string;
}

export function Post(
    { sortCol, date, replies, boosts, stars, content, media, mediaCount, url
    }: PostProps
) {
    const displayDate = new Date(date).toLocaleDateString();
    const getClass = (field: string, extraClass: string) => 'post-stat'
        + (extraClass ? ` ${extraClass}` : '')
        + (field == sortCol ? ' selected' : '');

    // `content` is sanitized by the Mastodon server
    return (
        <div class="post-row">
            <div class="post-deets">
                <div class="post-date" aria-label="post date and link">
                    <a href={url} target="_blank" rel="noreferrer">
                        {displayDate}
                    </a>
                </div>
                <div class="post-stats">
                    <div class={getClass('favourites_count', '')} aria-label="favourites">{stars}&nbsp;⭐️</div>
                    <div class={getClass('reblogs_count', '')} aria-label="bosts">{boosts}&nbsp;🚀</div>
                    <div class={getClass('replies_count', '')} aria-label="replies">{replies}&nbsp;🗣️</div>
                </div>
            </div>
            <div class="post-body">
                <div class="post-media">
                    <PostMedia media={media} mediaCount={mediaCount} />
                </div>
                <div class="post-content">
                    <div class="post-html"
                        dangerouslySetInnerHTML={{ __html: content }} /> {/* eslint-disable-line react/no-danger,react/jsx-closing-bracket-location */}
                </div>
            </div>
        </div>
    );
}
