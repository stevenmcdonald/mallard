import '../css/Account.css';

interface AccountProps {
    server: string;
    avatar: string;
    header: string;
    name: string;
    acct: string;
}

export function Account({ server, avatar, header, name, acct }: AccountProps) {
    const profileLink = `https://${server}/@${acct}`;
    const editProfile = `https://${server}/settings/profile`;

    return (
        <div class="account-background" style={`background-image: url(${header})`}>
            <div class="account">
                <div class="account-card">
                    <div class="account-avatar"><img src={avatar} /></div>
                    <div class="account-info">
                        <div class="account-name">{name}</div>
                        <div class="account-acct">
                            <a href={profileLink}
                                aria-label="Profile"
                                target="_blank"
                                rel="noreferrer"
                            >@{acct}</a>
                        </div>
                        <div class="account-edit">
                            <a href={editProfile}
                                aria-label="Edit profile"
                                target="_blank"
                                rel="noreferrer"
                            >edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
