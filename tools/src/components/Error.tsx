
interface ErrorProps {
    status?: number;
    message?: string;
}

export function Error({ status, message }: ErrorProps) {
    return (
        <div class="api-error">
            { status ? (<span>🔥{status == 429 ? 'Rate limited!' : status}🔥</span>) : '' }
            { message ? (<span>🔥 {message} 🔥</span>) : ''}
        </div>
    );
}
