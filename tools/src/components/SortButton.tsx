
interface SortButtonProps {
    label: string;
    labelExtra: string;
    ariaLabel: string;
    myCol: string;
    sortCol: string;
    onClick: (e: Event) => null;
}


export function SortButton({ label, labelExtra, ariaLabel, myCol, sortCol, onClick }: SortButtonProps) {
    const selected = myCol == sortCol;
    const cls = 'sort-button' + (selected ? ' selected' : '');
    const tempAriaLabel = selected ? ariaLabel + ' Selected' : ariaLabel;

    return (
        <button
            class={cls}
            type="button"
            onClick={onClick}
            aria-label={tempAriaLabel}
        >
            {label}<span class="desktop">&nbsp;{labelExtra}</span>
        </button>
    );
}
