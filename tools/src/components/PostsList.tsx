
import { Post } from './Post';
import { PostIface } from '../lib/mastodon';

import '../css/PostsList.css';

interface PostsListProps {
    sortCol: string;
    posts: PostIface[];
}

export function PostsList({ posts, sortCol }: PostsListProps) {
    if (posts.length == 0) {
        return (
            <div class="posts-list">
            No posts
            </div>
        );
    }

    return (
        <div class="posts-list">
            {posts.map((post) => {
                const media = post.media_attachments.length ?
                    post.media_attachments[0] : null;

                return (
                    <Post
                        sortCol={sortCol}
                        date={post.created_at}
                        replies={post.replies_count}
                        boosts={post.reblogs_count}
                        stars={post.favourites_count}
                        content={post.content}
                        media={media}
                        mediaCount={post.media_attachments.length}
                        url={post.url}
                    />
                );
            })}
        </div>
    );
}