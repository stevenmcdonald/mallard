
import { SortButton } from './SortButton';

// Styled in Controls.css

interface SortControlsProps {
    sortCol: string;
    sortDate: (_: Event) => null;
    sortStar: (_: Event) => null;
    sortBoost: (_: Event) => null;
    sortReply: (_: Event) => null;
}

export function SortControls({ sortCol, sortDate, sortStar, sortBoost, sortReply }: SortControlsProps) {
    return (
        <>
            <div class="sort-controls">
                <SortButton
                    label="📆"
                    labelExtra="date"
                    ariaLabel="Sort by date"
                    myCol={'created_at'}
                    sortCol={sortCol}
                    onClick={sortDate}
                />
                <SortButton
                    label="⭐️"
                    labelExtra="stars"
                    ariaLabel="Sort by favourites"
                    myCol="favourites_count"
                    sortCol={sortCol}
                    onClick={sortStar}
                />
                <SortButton
                    label="🚀"
                    labelExtra="boosts"
                    ariaLabel="Sort by boosts"
                    myCol="reblogs_count"
                    sortCol={sortCol}
                    onClick={sortBoost}
                />
                <SortButton
                    label="🗣️"
                    labelExtra="replies"
                    ariaLabel="Sort by replies"
                    myCol="replies_count"
                    sortCol={sortCol}
                    onClick={sortReply}
                />
            </div>
        </>
    );
}
