
import '../css/Loading.css';

export function Loading() {
    return (
        <div class="loading-container">
            <div class="loading">
                🌈&nbsp;Loading…&nbsp;🌈
            </div>
        </div>
    );
}