import { useEffect, useState } from 'preact/hooks';

import { Account } from './Account';
import { Error } from './Error';
import { Login } from './Login';
import { Navbar } from './Navbar';
import { PostsAPI } from './PostsAPI';

import { getUserInfo, AccountIface } from '../lib/mastodon';

// import { log } from '../lib/log';

import '../css/MallardPosts.css';

/*
    This is the root of the app. Handing the login logic and server, token, and
    post data is done here.
*/
export function MallardPosts() {
    const params = new URLSearchParams(location.search);
    const serverParam = params.get('server');
    const tokenParam = params.get('token');

    const [server, setServer] = useState<string | null>(null);
    const [token, setToken] = useState<string | null>(null);

    const [loading, setLoading] = useState(true);
    const [errorStatus, setErrorStatus] = useState<string | null>(null);
    const [account, setAccount] = useState<AccountIface | null>(null);


    // get params take prescedent over localStorage
    // we save later after the call to /verify_credentials/
    if (serverParam && tokenParam) {
        setServer(serverParam);
        setToken(tokenParam);
        // clear the params off the URL
        history.replaceState(null, '', '/');
    } else {
        const serverTmp = localStorage.getItem('server');
        const tokenTmp = localStorage.getItem('token');

        if (serverTmp && tokenTmp) {
            setServer(serverTmp);
            setToken(tokenTmp);
        }
    }

    if (!server || !token) {
        return (<Login />);
    }

    useEffect(() => {
        if (!server || !token) return;

        getUserInfo(server, token).then((userInfo) => {
            setLoading(false);
            if (!userInfo) {
                setErrorStatus('Error loading user');
                return;
            }

            // save these after we've verified they work
            localStorage.setItem('server', server);
            localStorage.setItem('token', token);
            setAccount(userInfo);
        });
    }, [server, token]);

    if (errorStatus) { return (<Error message={errorStatus} />); }
    if (loading || !account) { return (<div>Loading...</div>); }

    const uid = account ? account.id : '0';

    return (
        <div class="mallard">
            <Navbar />
            <Account
                server={server}
                avatar={account.avatar_static}
                header={account.header_static}
                name={account.display_name}
                acct={account.acct}
            />
            <PostsAPI
                server={server}
                token={token}
                userID={uid}
            />
        </div>
    );
}
