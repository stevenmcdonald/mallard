import { useCallback, StateUpdater } from 'preact/hooks';

// styled in Controls.css

interface SearchControlsProps {
    searchVal: string;
    setSearchVal: StateUpdater<string>;
    size: number;
}

export function SearchControls({ searchVal, setSearchVal, size }: SearchControlsProps) {

    const onInput = useCallback((e: Event) => {
        const element = e.target as HTMLInputElement;
        setSearchVal(element.value);
    }, [searchVal]);

    return   (
        <div class="search-controls">
            <input
                id="search-input"
                aria-label="Filter posts"
                class="search-input"
                type="search"
                placeholder="Type to filter posts"
                value={searchVal}
                onInput={onInput}
                size={size}
            />
        </div>
    );
}
