
import { useCallback, useMemo, useState } from 'preact/hooks';

import { SortedPostsList } from './SortedPostsList';

import { Controls } from './Controls';
import { Loading } from './Loading';
import { PostsManager } from '../lib/PostsManager';
import { PostIface } from '../lib/mastodon';


interface PostsAPIProps {
    server: string | null;
    token: string | null;
    userID: string | null;
}

export function PostsAPI({ server, token, userID }: PostsAPIProps) {
    const [loading, setLoading] = useState(true);

    if (!server || !token || !userID) {
        return (<Loading />);
    }

    const [posts, setPosts] = useState<PostIface[] | []>([]);
    const [activeTab, setActiveTab] = useState('sort');
    const [sortCol, setSortCol] = useState('created_at');
    const [searchVal, setSearchVal] = useState('');
    const [excludeReplies, setExcludeReplies] = useState(true);
    const [onlyMedia, setOnlyMedia] = useState(false);

    const mgr = useMemo(
        () => new PostsManager(server, token, userID),
        [server, token, userID]
    );

    useMemo(async () => {
        setLoading(true);
        // assure the manager is in a conssstent state
        mgr.excludeBoosts(true)
            .excludeReplies(excludeReplies)
            .onlyMedia(onlyMedia);

        const p = await mgr.getPosts();
        setLoading(false);
        setPosts(p);
    }, [excludeReplies, onlyMedia]);

    const setIncludeReplies = useCallback(
        (v: boolean) => setExcludeReplies(!v),
        [excludeReplies]
    );

    const changeSort = useCallback((col: string) => {
        if (sortCol == col) {
            return;
        }
        setSortCol(col);
    }, [sortCol]);

    // I can't just inline this because it has to return null,
    // is there a better thing to do here?
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const getChangeSort = (s: string) => (_: Event) => {
        changeSort(s);
        return null; // not void
    };

    const sortDate = useCallback(getChangeSort('created_at'), [sortCol]);
    const sortStar = useCallback(getChangeSort('favourites_count'), [sortCol]);
    const sortBoost = useCallback(getChangeSort('reblogs_count'), [sortCol]);
    const sortReply = useCallback(getChangeSort('replies_count'), [sortCol]);

    return (
        <>
            <div class="posts-controls-container">
                <Controls
                    activeTab={activeTab}
                    setActiveTab={setActiveTab}
                    sortCol={sortCol}
                    sortDate={sortDate}
                    sortStar={sortStar}
                    sortBoost={sortBoost}
                    sortReply={sortReply}
                    searchVal={searchVal}
                    setSearchVal={setSearchVal}
                    includeReplies={!excludeReplies}
                    setIncludeReplies={setIncludeReplies}
                    onlyMedia={onlyMedia}
                    setOnlyMedia={setOnlyMedia}
                />
            </div>
            { loading
                ? (<Loading />)
                : (
                    <SortedPostsList
                        posts={posts}
                        sortCol={sortCol}
                        searchVal={searchVal}
                    />
                )
            }

        </>
    );
}
