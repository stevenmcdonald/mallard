
// import { log } from '../lib/log';

import { MediaAttachmentIface } from '../lib/mastodon';

interface PostMediaProps {
    media: MediaAttachmentIface | null;
    mediaCount: number;
}

export function PostMedia({ media, mediaCount }: PostMediaProps) {
    let cls = mediaCount > 1 ? 'multiple' : '';

    if (!mediaCount || !media) return null;

    switch (media.type) {
    case 'image':
        cls = cls + ' post-image';
        return (
            <div class={cls + 'post-image'}>
                {/* eslint-disable-next-line camelcase*/}
                <img height="96" width="96" src={media.preview_url} alt={media.description} />
            </div>
        );
    default:
        return null;
    }

}