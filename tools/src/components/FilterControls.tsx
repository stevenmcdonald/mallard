
import { useCallback, StateUpdater } from 'preact/hooks';

interface FilterControlsProps {
    includeReplies: boolean;
    setIncludeReplies: (v: boolean) => void;
    onlyMedia: boolean;
    setOnlyMedia: StateUpdater<boolean>;
}

export function FilterControls({
    includeReplies, setIncludeReplies, onlyMedia, setOnlyMedia
}: FilterControlsProps) {

    const toggleReplies = useCallback(
        (_: Event) => setIncludeReplies(!includeReplies),
        [includeReplies]
    );
    const toggleMedia = useCallback(
        (_: Event) => setOnlyMedia(!onlyMedia), [onlyMedia]);

    const repliesCls = 'filter-button' + (includeReplies ? ' selected' : '');
    const mediaCls = 'filter-button' + (onlyMedia ? ' selected' : '');

    return (
        <div class="filter-contorls">
            <button
                type="button"
                class={repliesCls}
                onClick={toggleReplies}
            >
            Replies
            </button>
            <button
                type="button"
                class={mediaCls}
                onClick={toggleMedia}
            >
            Only Media
            </button>
        </div>
    );
}