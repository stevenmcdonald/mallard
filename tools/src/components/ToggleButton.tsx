
import { useCallback, StateUpdater } from 'preact/hooks';
import { log } from '../lib/log';


interface ToggleButtonProps {
    options: string[];
    labels: string[];
    ariaLabels: string[];
    activeOption: string;
    setOption: StateUpdater<string>;
}

function safeIdxOf<T>(a: T[], v: T) {
    const i = a.indexOf(v);
    if (i == -1) {
        log.error('invalid index!');
        return 0;
    }
    return i;
}

export function ToggleButton({ options, labels, ariaLabels, activeOption, setOption }: ToggleButtonProps) {
    if (options.length != labels.length && options.length != ariaLabels.length) {
        log.error('options, labels, and ariaLabels need to be the same length');
        return null;
    }

    const index = safeIdxOf(options, activeOption);

    const onClick = useCallback(() => {
        let nextIndex = index + 1;
        if (nextIndex > options.length - 1) {
            nextIndex = 0;
        }

        setOption(options[nextIndex]);
    }, [activeOption]);

    return (
        <button
            class="toggle-button"
            aria-label={ariaLabels[index]}
            type="button"
            onClick={onClick}
        >
            {ariaLabels[index]}&nbsp;{labels[index]}
        </button>
    );
}