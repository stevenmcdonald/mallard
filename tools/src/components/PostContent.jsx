/*
I converted this to JS rather than convince TS that
this.base.innerHTML exists and is the right type
*/

import { Component } from 'preact';

// from the example here:
// https://github.com/preactjs/preact/issues/29#issuecomment-176444459
export class PostContent extends Component {
    constructor({ html }) {
        super();
        this.html = html;
    }

    componentDidMount() {
        this.base.innerHTML = this.html;
    }

    render() {
        return null;
    }
}
