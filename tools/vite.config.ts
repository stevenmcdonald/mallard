/// <reference types="vitest" />
import { defineConfig } from 'vite';
import { configDefaults } from 'vitest/config';
import preact from '@preact/preset-vite';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        preact()
    ],
    server: {
        proxy: {
            '/api': {
                target: 'http://localhost:8000/',
                changeOrigin: true
            }
        }
    },
    test: {
        environment: 'happy-dom',
        exclude: [...configDefaults.exclude],
        setupFiles: [
            './setupVitest.js'
        ]
    }
});
