import asyncio
import logging
import urllib.parse

from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import View

from login.models import OAuthApp
from login.utils import register_app, get_user_token, get_redirect_uri

# this entirely exists to handle the OAuth login flow

logger = logging.getLogger(__name__)


class LoginView(View):
    async def get(self, request, uri):
        # should this be properly validated? escaping it prevents the
        # worst problems
        uri = urllib.parse.quote(uri, safe='')

        try:
            app = await OAuthApp.objects.aget(uri=uri)
            logger.debug(f'loaded existing app for {uri}')
        except OAuthApp.DoesNotExist:
            logger.debug(f'registering app with {uri}')
            app = await register_app(uri)

        redirect_uri = get_redirect_uri(uri)

        params = {
            'response_type': 'code',
            'client_id': app.client_id,
            'redirect_uri': redirect_uri,
            'scope': 'read',
        }

        redirect_to = f'https://{uri}/oauth/authorize?' + \
            urllib.parse.urlencode(params)

        return HttpResponseRedirect(redirect_to)


class OAuthDoneView(View):
    async def get(self, request, uri):
        code = request.GET.get('code')
        if not code:
            return HttpResponse('no code', status_code=400)

        token = await get_user_token(uri, code)

        # redirect_to = reverse('js-app', uri=uri, token=token)
        redirect_to = f'/?server={uri}&token={token}'

        response = HttpResponseRedirect(redirect_to)
        response.headers['Cache-Control'] = 'private'
        return response
