import json
from unittest.mock import AsyncMock, Mock, patch

from django.conf import settings
from django.http import Http404
from django.test import AsyncClient, TestCase

from login.models import OAuthApp
from login.utils import register_app, get_user_token, get_redirect_uri


oauth_app_mock = json.loads("""
{
    "client_id": "RBWxfKnAe2MfohiisBzAe28EvJv2ZGS9lDk5srgOQZQ",
    "client_secret": "2N1ZrwRKBGUsccNBDSxFIbYQEBeNcKl3aiDWEtlUKQc",
    "id": "123456",
    "name": "mallard-test",
    "redirect_uri": "https://example.online/oauth/example.social",
    "vapid_key": "BIqC7gExWl9KYi9D89xF2SaYYmEA0BVO8hZizOOEu390mhjFUqfwysW3sEkklpjfIevoVY_2OkIFg8O_pGZGBMA=",
    "website": "http://example.online/mallard-test/"
}
""")

# TODO get a more real response
mock_token_rep = json.loads("""
{
    "access_token": "1234567890987654321"
}
""")


class UtilsTestCase(TestCase):
    def setUp(self):
        self.code = "99999999998888888888"

    @patch('login.utils.aiohttp.ClientSession.post')
    async def test_register_app(self, mock_post):
        mock_post.return_value.__aenter__.return_value.json = AsyncMock()
        mock_post.return_value.__aenter__.return_value.status = 200
        mock_post.return_value.__aenter__.return_value.json.return_value = oauth_app_mock

        # neigher the decorator nor the context manager was correctly
        # creating this as an AsyncMock automatically 🤷
        mock_app = Mock()

        mock_agoc = AsyncMock()
        mock_agoc.return_value = (mock_app, False)
        with patch('login.utils.OAuthApp.objects.aget_or_create', new=mock_agoc):
            app = await register_app('example.social')

        self.assertEqual(app, mock_app)

        expected_data = {
            'client_name': settings.MALLARD_OAUTH_CLIENT_NAME,
            'redirect_uris': settings.MALLARD_BASE_URL + '/api/oauth/example.social/',
            'scopes': 'read',
            'website': settings.MALLARD_WEBSITE,
        }

        mock_post.assert_called_with('https://example.social/api/v1/apps',
            data=expected_data)
        mock_agoc.assert_called_with(uri='example.social',
            client_id=oauth_app_mock['client_id'],
            client_secret=oauth_app_mock['client_secret'],
            vapid_key=oauth_app_mock['vapid_key'])

    @patch('login.utils.OAuthApp.objects.aget_or_create')
    @patch('login.utils.aiohttp.ClientSession.post')
    async def test_register_app_error(self, mock_post, moc_agoc):
        mock_post.return_value.__aenter__.return_value.status = 404

        app = await register_app('example.world')

        self.assertEqual(app, None)
        moc_agoc.assert_not_called()

    @patch('login.utils.aiohttp.ClientSession.post')
    async def test_get_user_token(self, mock_post):
        mock_post.return_value.__aenter__.return_value.json = AsyncMock()
        mock_post.return_value.__aenter__.return_value.status = 200
        mock_post.return_value.__aenter__.return_value.json.return_value = mock_token_rep

        mock_app = Mock()
        mock_app.client_id = oauth_app_mock['client_id']
        mock_app.client_secret = oauth_app_mock['client_secret']

        mock_aget = AsyncMock()
        mock_aget.return_value = mock_app
        with patch('login.utils.OAuthApp.objects.aget', new=mock_aget):
            token = await get_user_token('example.social', self.code)

        self.assertEqual(token, mock_token_rep['access_token'])

        expected_data = {
            'client_id': oauth_app_mock['client_id'],
            'client_secret': oauth_app_mock['client_secret'],
            'redirect_uri': get_redirect_uri('example.social'),
            'grant_type': 'authorization_code',
            'code': self.code,
            'scope': 'read',
        }

        mock_aget.assert_called_with(uri='example.social')
        mock_post.assert_called_with('https://example.social/oauth/token',
                                     data=expected_data)

    @patch('login.utils.aiohttp.ClientSession.post')
    async def test_get_user_token_no_app(self, mock_post):

        mock_aget = AsyncMock(side_effect=OAuthApp.DoesNotExist)

        with patch('login.utils.OAuthApp.objects.aget', new=mock_aget):
            with self.assertRaises(Http404):
                await get_user_token('doesnotexist.example.com', self.code)

        mock_post.assert_not_called()
