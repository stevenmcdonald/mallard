import urllib.parse
from unittest.mock import patch, Mock

from django.conf import settings
from django.test import AsyncClient, TestCase

from login.models import OAuthApp
from login.utils import register_app


class LoginTestCase(TestCase):
    def setUp(self):
        self.client = AsyncClient()

        self.fake_instance = {
            'uri': 'example.social',
            'client_id': '0123456789',
            'client_secret': '0987654321',
            'vapid_key': 'acb123xyz890'
        }

    async def test_login_no_app(self):
        mock_app = Mock()
        mock_app.client_id = self.fake_instance['client_id']

        with patch('login.views.register_app', return_value=mock_app) as mock_register_app:
            r = await self.client.get('/api/login/example.social/')

            mock_register_app.assert_called_with('example.social')

            expected_url = 'https://example.social/oauth/authorize?' + \
                urllib.parse.urlencode({
                    'response_type': 'code',
                    'client_id': self.fake_instance['client_id'],
                    'redirect_uri': settings.MALLARD_BASE_URL + '/api/oauth/example.social/',
                    'scope': 'read'})

            self.assertRedirects(r, expected_url, fetch_redirect_response=False)

    async def test_login_with_app(self):

        app = await OAuthApp.objects.acreate(**self.fake_instance)

        with patch('login.views.register_app') as mock_register_app:
            r = await self.client.get('/api/login/example.social/')

            mock_register_app.assert_not_called()

            expected_url = 'https://example.social/oauth/authorize?' + \
                urllib.parse.urlencode({
                    'response_type': 'code',
                    'client_id': self.fake_instance['client_id'],
                    'redirect_uri': settings.MALLARD_BASE_URL + '/api/oauth/example.social/',
                    'scope': 'read'})

            self.assertRedirects(r, expected_url, fetch_redirect_response=False)


class OAuthDoneView(TestCase):
    def setUp(self):
        self.client = AsyncClient()

    async def test_no_code(self):
        with patch('login.views.get_user_token', return_value='fake_token') as mock_get_user_token:
            r = await self.client.get('/api/oauth/example.social/?code=fake_code')

            mock_get_user_token.assert_called_with('example.social', 'fake_code')

            expected_url = '/?' + urllib.parse.urlencode({
                'server': 'example.social',
                'token': 'fake_token'})

            self.assertRedirects(r, expected_url, fetch_redirect_response=False)
