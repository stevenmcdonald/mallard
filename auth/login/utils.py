import aiohttp
import logging
from pprint import pprint

from django.conf import settings
from django.http import Http404
from django.urls import reverse

from login.models import OAuthApp

logger = logging.getLogger(__name__)


def get_redirect_uri(uri):
    # XXX
    return settings.MALLARD_BASE_URL + \
        reverse('oauth-redirect', args=(uri,))


async def register_app(uri):
    register_url = f'https://{uri}/api/v1/apps'

    redirect_uris = get_redirect_uri(uri)

    data = {
        'client_name': settings.MALLARD_OAUTH_CLIENT_NAME,
        'redirect_uris': redirect_uris,
        'scopes': 'read',
        'website': settings.MALLARD_WEBSITE,
    }

    logger.info(f'registering app with {uri}...', data)

    async with aiohttp.ClientSession() as s:
        async with s.post(register_url, data=data) as r:
            if r.status != 200:
                logger.error(f'register app error {uri} {r.status}')
                return None

            d = await r.json()

            app, _ = await OAuthApp.objects.aget_or_create(
                uri=uri, client_id=d['client_id'],
                client_secret=d['client_secret'],
                vapid_key=d['vapid_key'])

            return app


async def get_user_token(uri, code):
    try:
        app = await OAuthApp.objects.aget(uri=uri)
    except OAuthApp.DoesNotExist:
        raise Http404()

    data = {
        'client_id': app.client_id,
        'client_secret': app.client_secret,
        'redirect_uri': get_redirect_uri(uri),
        'grant_type': 'authorization_code',
        'code': code,
        'scope': 'read',
    }

    logger.debug(f'getting user token from {uri}...', data);

    token_url = f'https://{uri}/oauth/token'

    async with aiohttp.ClientSession(raise_for_status=True) as s:
        async with s.post(token_url, data=data) as r:
            data = await r.json()
            logger.info('token data:', data)
            return data['access_token']
