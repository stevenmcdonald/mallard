from django.db import models


class OAuthApp(models.Model):
    uri = models.TextField(db_index=True)
    client_id = models.TextField()
    client_secret = models.TextField()
    vapid_key = models.TextField()


