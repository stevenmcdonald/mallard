
Mallard Tools
=============

For now, this is a simple web app to see your own Mastodon posts, sort them by stats, and soon, search. Also media support.

The bulk of the code is written in Preact and Typescript. There's a small Django backend to handle OAuth app registration, it's two simple views and one simple model.

This exists because I couldn't find any way to see which of my own posts were the most popular, and to find recent posts. The default web UI doesn't help much.


dev setup
---------

* Django:

create a .env file in auth/ or edit auth/settings.py to your liking

```sh
cd auth
python3 -m venv venv
. ./venv/bin/activate
pip install -r requirements.txt
pip install -r requirements_dev.txt
./manage.py runserver
./manage.py test # to test
```

* Vite:

```sh
cd tools
npm install
npm run dev
npx vitest # to test
```

Vite will proxy to Django on the default port with the included vite config
